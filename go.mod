module bitbucket.org/uwaploe/gennmea

require (
	bitbucket.org/mfkenney/go-nmea v1.1.0
	bitbucket.org/uwaploe/go-focus v0.1.0
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/nats-io/go-nats v1.7.2 // indirect
	github.com/nats-io/go-nats-streaming v0.4.4
	github.com/nats-io/nkeys v0.1.0 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
)
