// Gennmea generates NMEA messages for the navigation software (Hypack)
// from information in the PC Client String provided by the FOCUS towbody.
package main

import (
	"bytes"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"

	nmea "bitbucket.org/mfkenney/go-nmea"
	focus "bitbucket.org/uwaploe/go-focus"
	stan "github.com/nats-io/go-nats-streaming"
)

var Version = "dev"
var BuildDate = "unknown"

func generateNmea(rec map[string]float32) []nmea.Sentence {
}

func main() {
	natsURL := os.Getenv("NATS_URL")
	clusterID := os.Getenv("NATS_CLUSTER_ID")

	sc, err := stan.Connect(clusterID, "gen-nmea", stan.NatsURL(natsURL))
	if err != nil {
		log.Fatalf("Cannot connect: %v", err)
	}
	defer sc.Close()

	focusCb := func(m *stan.Msg) {
		var sentence string
		// Accept tagged or untagged NMEA messages produced by nmeapub.
		bs := bytes.SplitN(m.Data, []byte(":"), 2)
		if len(bs) == 2 {
			sentence = string(bs[1])
		} else {
			sentence = string(bs[0])
		}
		rec, err := focus.ParseRecord(sentence)
		if err != nil {
			log.Printf("FOCUS data parse error: %v", err)
		}
		for _, s := range generateNmea(rec) {
			sc.Publish("nmea."+strings.ToLower(s.Id[2:]),
				[]byte(tag+":"+s.String()))
			sc.Publish("nmea.focus", []byte(s.String()))
		}
	}

	focusSub, err := sc.Subscribe(os.Getenv("FOCUS_SUBJECT"), focusCb,
		stan.StartWithLastReceived())
	if err != nil {
		log.Fatalf("Cannot subscribe to FOCUS data: %v", err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	done := make(chan bool)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			focusSub.Unsubscribe()
			done <- true
		}
	}()

	log.Printf("NMEA generating service starting %s", Version)

	<-done
}
